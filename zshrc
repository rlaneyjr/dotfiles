#######################################
#               ZSHRC                 #
#######################################
export TERM='xterm-256color'
#export ZSH=$HOME/.oh-my-zsh
# This uses antigens oh-my-zsh and works both Linux & MAC.
#export ZSH=$HOME/.antigen/repos/https-COLON--SLASH--SLASH-github.com-SLASH-robbyrussell-SLASH-oh-my-zsh.git
export ZSH=$HOME/.zplug/repos/robbyrussell/oh-my-zsh
export ZSH_CUSTOM=$HOME/.zsh
source $ZSH/oh-my-zsh.sh
source $HOME/.zshenv


##############################################
#
#		Powerlevel9k Config
#
##############################################

#POWERLEVEL9K_MODE='compatible'
#POWERLEVEL9K_MODE='awesome-patched'
#POWERLEVEL9K_MODE='awesome-fontconfig'
#POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(virtualenv context dir vi_mode rbenv)
#POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status history time)
#export AWS_DEFAULT_PROFILE=rlaneyjr@gmail.com
POWERLEVEL9K_SHORTEN_DIR_LENGTH=3
#POWERLEVEL9K_PROMPT_ON_NEWLINE=true
#POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX="↱"
POWERLEVEL9K_MULTILINE_SECOND_PROMPT_PREFIX="↳ "
#POWERLEVEL9K_DISABLE_RPROMPT=true
POWERLEVEL9K_COLOR_SCHEME='dark'
#POWERLEVEL9K_TIME_FOREGROUND='red'
#POWERLEVEL9K_TIME_BACKGROUND='blue'

# Custom config example 1
#POWERLEVEL9K_MODE='awesome-patched'
#POWERLEVEL9K_SHORTEN_DIR_LENGTH=3
POWERLEVEL9K_SHORTEN_STRATEGY="truncate_from_left"
POWERLEVEL9K_STATUS_VERBOSE=true
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(status context dir vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(virtualenv time)
#POWERLEVEL9K_SHOW_CHANGESET=true
#POWERLEVEL9K_CHANGESET_HASH_LENGTH=6

# Custom config example 2
#POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(os_icon load context dir node_version vcs)
#POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(longstatus history time)
POWERLEVEL9K_TIME_FORMAT="%D{%H:%M:%S %m/%d/%Y}"
#POWERLEVEL9K_NODE_VERSION_BACKGROUND='022'

# Customizing `context` colors for root and non-root users
#POWERLEVEL9K_CONTEXT_DEFAULT_BACKGROUND="green"
#POWERLEVEL9K_CONTEXT_DEFAULT_FOREGROUND="cyan"
#POWERLEVEL9K_CONTEXT_ROOT_BACKGROUND="red"
#POWERLEVEL9K_CONTEXT_ROOT_FOREGROUND="blue"

# Advanced `vcs` color customization
#POWERLEVEL9K_VCS_FOREGROUND='blue'
#POWERLEVEL9K_VCS_DARK_FOREGROUND='black'
#POWERLEVEL9K_VCS_BACKGROUND='green'
# If VCS changes are detected:
#POWERLEVEL9K_VCS_MODIFIED_FOREGROUND='red'
#POWERLEVEL9K_VCS_MODIFIED_BACKGROUND='cyan'

##############################################
#
#		        Zplug Config
#
##############################################
source ~/.zplug/zplug

# Make sure you use double quotes
zplug "zsh-users/zsh-history-substring-search"
zplug "bhilburn/powerlevel9k"

# Support oh-my-zsh plugins and the like
# Bundles from the default repo (robbyrussell's oh-my-zsh).
zplug "plugins/git",   from:oh-my-zsh, if:"which git"
#zplug "themes/duellj", from:oh-my-zsh
zplug "lib/clipboard", from:oh-my-zsh, if:"[[ $OSTYPE == *darwin* ]]"
zplug "plugins/osx",   from:oh-my-zsh, if:"[[ $OSTYPE == *darwin* ]]"
zplug "plugins/brew",   from:oh-my-zsh, if:"[[ $OSTYPE == *darwin* ]]"
zplug "plugins/command-not-found",   from:oh-my-zsh
zplug "plugins/vi-mode",   from:oh-my-zsh
zplug "plugins/colorize",   from:oh-my-zsh
#zplug "plugins/git",   from:oh-my-zsh
zplug "plugins/github",   from:oh-my-zsh
zplug "plugins/ssh-agent",   from:oh-my-zsh
zplug "plugins/tmux",   from:oh-my-zsh
zplug "plugins/tmuxinator",   from:oh-my-zsh

# Python Bundles
zplug "plugins/pip",   from:oh-my-zsh
zplug "plugins/python",   from:oh-my-zsh
zplug "plugins/virtualenv",   from:oh-my-zsh
zplug "plugins/virtualenvwrapper",   from:oh-my-zsh
zplug "plugins/pyenv",   from:oh-my-zsh
zplug "plugins/django",   from:oh-my-zsh
zplug "plugins/fabric",   from:oh-my-zsh

# Cloud Bundles
zplug "plugins/vagrant",   from:oh-my-zsh
zplug "plugins/docker",   from:oh-my-zsh
zplug "plugins/docker-compose",   from:oh-my-zsh
zplug "plugins/heroku",   from:oh-my-zsh

# Can manage local plugins
#zplug "~/.zsh/autoenv", from:local
#zplug "~/.zsh/zsh-autoenv", from:local
# Bundles that need work!!
#zplug "kennethreitz/autoenv"
zplug "Tarrasch/zsh-autoenv"

# Set priority to load command like a nice command
# e.g., zsh-syntax-highlighting must be loaded
# after executing compinit command and sourcing other plugins
# Syntax highlighting bundle.
zplug "zsh-users/zsh-syntax-highlighting", nice:10

# A relative path is resolved with respect to the $ZPLUG_HOME
#zplug "repos/robbyrussell/oh-my-zsh/custom/plugins/my-plugin", from:local

# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# Then, source plugins and add commands to $PATH
zplug load --verbose

##############################################
#
#		Custom Config
#
##############################################
## load our own completion functions
#fpath=(~/.zsh/completion /usr/local/share/zsh-completions /usr/local/share/zsh/site-functions $fpath)
#
## completion
#autoload -U compinit
#compinit

# load custom executable functions
for function in ~/.zsh/functions/*; do
  source $function
done

# aliases
[[ -f ~/.aliases ]] && source ~/.aliases

## extra files in ~/.zsh/configs/pre , ~/.zsh/configs , and ~/.zsh/configs/post
## these are loaded first, second, and third, respectively.
#_load_settings() {
#  _dir="$1"
#  if [ -d "$_dir" ]; then
#    if [ -d "$_dir/pre" ]; then
#      for config in "$_dir"/pre/**/*(N-.); do
#        . $config
#      done
#    fi
#
#    for config in "$_dir"/**/*(N-.); do
#      case "$config" in
#        "$_dir"/pre/*)
#          :
#          ;;
#        "$_dir"/post/*)
#          :
#          ;;
#        *)
#          if [ -f $config ]; then
#            . $config
#          fi
#          ;;
#      esac
#    done
#
#    if [ -d "$_dir/post" ]; then
#      for config in "$_dir"/post/**/*(N-.); do
#        . $config
#      done
#    fi
#  fi
#}
#_load_settings "$HOME/.zsh/configs"

##############################################
### Other zplug examples ###
## Can manage a plugin as a command
## And accept glob patterns (e.g., brace, wildcard, ...)
#zplug "Jxck/dotfiles", as:command, of:"bin/{histuniq,color}"
#
## Can manage everything e.g., other person's zshrc
#zplug "tcnksm/docker-alias", of:zshrc
#
## Prohibit updates to a plugin by using the "frozen:" tag
#zplug "k4rthik/git-cal", as:command, frozen:1
#
## Grab binaries from GitHub Releases
## and rename to use "file:" tag
#zplug "junegunn/fzf-bin", \
#    as:command, \
#    from:gh-r, \
#    file:fzf, \
#    of:"*darwin*amd64*"

## Run a command after a plugin is installed/updated
#zplug "tj/n", do:"make install"
#
## Support checking out a specific branch/tag/commit of a plugin
#zplug "b4b4r07/enhancd", at:v1
#zplug "mollifier/anyframe", commit:4c23cb60
#
## Install if "if:" tag returns true
#zplug "hchbaw/opp.zsh", if:"(( ${ZSH_VERSION%%.*} < 5 ))"
#
## Can manage gist file just like other plugins
#zplug "b4b4r07/79ee61f7c140c63d2786", \
#    from:gist, \
#    as:command, \
#    of:get_last_pane_path.sh
#
## Support bitbucket
#zplug "b4b4r07/hello_bitbucket", \
#    as:command, \
#    from:bitbucket, \
#    do:"chmod 755 *.sh", \
#    of:"*.sh"
#
## Group dependencies, emoji-cli depends on jq in this example
#zplug "stedolan/jq", \
#    as:command, \
#    file:jq, \
#    from:gh-r \
#    | zplug "b4b4r07/emoji-cli"
