# Aliases
# This intitiates a database in the defined directory. The next command start the database server.
# initdb ~/DB/postgres -E utf8
# pg_ctl -D /Users/rlaney/DB/postgres -l logfile start
alias gopg='postgres -D /usr/local/var/postgres'
alias pgstart='launchctl load ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist'
alias pgstop='launchctl unload ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist'
alias cppcompile='c++ -std=c++11 -stdlib=libc++'

# Unix
alias ll="ls -al"
alias ln="ln -v"
alias mkdir="mkdir -p"
#alias e="$EDITOR"
#alias v="$VISUAL"

# Bundler
alias b="bundle"

# Most used and needed Aliases
#alias mvim='vim'
alias cl='clear'
alias e='mvim'
alias x='exit'
alias v='vim'
alias sv='sudo vim'
alias se='sudo mvim'
alias lmo='l | more'
alias les='l | less'

# Gui Aliases for "mvim/gvim"
alias ohmyzsh='mvim $HOME/.zplug/repos/robbyrussell/oh-my-zsh/oh-my-zsh.sh'
alias zshconfig='mvim $HOME/.zshrc'
alias envconfig='mvim $HOME/.zsh/configs/env.zsh'
alias antconfig='mvim $HOME/.antigen.zsh'
alias tmxconfig='mvim $HOME/.tmux.conf'
alias vimconfig='mvim $HOME/.vimrc'
alias evimrc='mvim $HOME/.vimrc'
alias sbtconfig='mvim $HOME/.sbtconfig'
alias zshconfig='mvim $HOME/.zshrc'

# Shell Aliases "vim"
alias omz='vim $HOME/.zplug/repos/robbyrussell/oh-my-zsh/oh-my-zsh.sh'
alias zconfig='vim $HOME/.zshrc'
alias econfig='vim $HOME/.zsh/configs/env.zsh'
alias aconfig='vim $HOME/.antigen.zsh'
alias tconfig='vim $HOME/.tmux.conf'
alias vconfig='vim $HOME/.vimrc'
alias evim='vim $HOME/.vimrc'
alias sconfig='vim $HOME/.sbtconfig'
alias zconfig='vim $HOME/.zshrc'

# Git aliases
alias gs='git status'
alias gc='git commit'
alias gcm='git commit -m'
alias gcam='git commit -a -m'
alias gp='git pull'
alias gpo='git push origin'
alias gpom='git push origin master'
alias ga='git add'
alias gaa='git add -A'
alias gr='git remote'
alias gcr='git create'
alias gh='git help'

# tmux aliases
alias ta='tmux attach'
alias tls='tmux ls'
alias tat='tmux attach -t'
alias tns='tmux new-session -s'
alias tx="tmuxinator"

# Python PIP aliases
alias pf="pip freeze"
alias pi="pip install"
alias pu="pip uninstall"

# Virtualenv aliases
# Use --relocate for relative paths
# Use --system-site-packages to install system packages
alias ve="virtualenv"
alias de="deactivate"

# Node package manager aliases
# To install a package:
alias npi="npm install" #<package> Install locally
alias npig="npm install -g" #<package> Install globally
# To install a package and save it in your project's package.json file:
alias npis="npm install "$1" --save"
# To see what's installed:
alias npl="npm list" # Local
alias nplg="npm list -g" # Global
# To find outdated packages (locally or globally):
alias npo="npm outdated"
alias npog="npm outdated -g"
# To upgrade all or a particular package:
alias npu="npm update"
# To uninstall a package:
alias npui="npm uninstall"

# Node version manager aliases
alias nv="nvm"  # check the nvm use message
alias nvis="nvm install stable"  # install most recent nodejs stable version
alias nvl="nvm ls"  # list installed node version
alias nvus="nvm use stable"  # use stable as current version
alias nvlr="nvm ls-remote"  # list all the node versions you can install
alias nvds="nvm alias default stable"  # set the installed stable version as the default node

# Rails
alias migrate="rake db:migrate db:rollback && rake db:migrate"
alias s="rspec"

# Pretty print the path
alias path='echo $PATH | tr -s ":" "\n"'

# Include custom aliases
#[[ -f ~/.aliases.local ]] && source ~/.aliases.local
